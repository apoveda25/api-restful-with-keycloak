namespace Tarker.Booking.Application.Database.Users.Queries.GetUserById;

public interface IGetUserByIdQuery
{
    Task<GetUserByIdDto?> ExecuteAsync(Guid userId);
}