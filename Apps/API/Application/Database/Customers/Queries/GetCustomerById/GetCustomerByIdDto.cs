namespace Tarker.Booking.Application.Database.Customers.Queries.GetCustomerById;

public class GetCustomerByIdDto
{
    public Guid CustomerId { get; set; }
    public string FullName { get; set; }
    public string DocumentNumber { get; set; }
}