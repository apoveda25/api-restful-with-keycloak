namespace Tarker.Booking.Application.Database.Users.Commands.UpdateUserPassword;

public class UpdateUserPasswordDto
{
    public Guid UserId { get; set; }
    public string Password { get; set; }
}