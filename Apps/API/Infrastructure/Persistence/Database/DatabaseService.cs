using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Tarker.Booking.Application.Database;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Infrastructure.Persistence.Database;

public class DatabaseService(DbContextOptions dbContextOptions) : DbContext(options: dbContextOptions), IDatabaseService
{
    public DbSet<UserEntity> User { get; set; }
    public DbSet<CustomerEntity> Customer { get; set; }
    public DbSet<BookingEntity> Booking { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // modelBuilder.ApplyConfiguration(configuration: new UserConfiguration());
        // modelBuilder.ApplyConfiguration(configuration: new CustomerConfiguration());
        // modelBuilder.ApplyConfiguration(configuration: new BookingConfiguration());
        modelBuilder.ApplyConfigurationsFromAssembly(assembly: Assembly.GetExecutingAssembly());
    }

    public async Task<bool> SaveAsync()
    {
        return await base.SaveChangesAsync() > 0;
    }
}