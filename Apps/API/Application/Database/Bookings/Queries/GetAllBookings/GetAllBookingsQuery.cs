using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Bookings.Queries.GetAllBookings;

public class GetAllBookingsQuery(IDatabaseService databaseService) : IGetAllBookingsQuery
{
    private readonly IDatabaseService databaseService = databaseService;

    public async Task<List<GetAllBookingsDto>> ExecuteAsync()
    {
        var result = await (from booking in databaseService.Booking
                            join customer in databaseService.Customer
                            on booking.CustomerId equals customer.CustomerId
                            select new GetAllBookingsDto
                            {
                                BookingId = booking.BookingId,
                                Code = booking.Code,
                                RegisterDate = booking.RegisterDate,
                                Type = booking.Type,
                                CustomerFullName = customer.FullName,
                                CustomerDocumentNumber = customer.DocumentNumber
                            }).ToListAsync();

        return result;
    }
}