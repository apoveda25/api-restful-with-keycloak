namespace Tarker.Booking.Application.Database.Bookings.Queries.GetBookingsByDocumentNumber;

public interface IGetBookingsByDocumentNumberQuery
{
    Task<List<GetBookingsByDocumentNumberDto>> ExecuteAsync(string documentNumber);
}