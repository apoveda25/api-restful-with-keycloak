using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Customers.Queries.GetAllCustomers;

public class GetAllCustomersQuery(IDatabaseService databaseService, IMapper mapper) : IGetAllCustomersQuery
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<List<GetAllCustomersDto>> ExecuteAsync()
    {
        var entities = await databaseService.Customer.ToListAsync();

        return mapper.Map<List<GetAllCustomersDto>>(source: entities);
    }
}