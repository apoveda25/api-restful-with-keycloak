using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Bookings.Queries.GetBookingsByType;

public class GetBookingsByTypeQuery(IDatabaseService databaseService) : IGetBookingsByTypeQuery
{
    private readonly IDatabaseService databaseService = databaseService;

    public async Task<List<GetBookingsByTypeDto>> ExecuteAsync(string type)
    {
        var result = await (from booking in databaseService.Booking
                            join customer in databaseService.Customer
                            on booking.CustomerId equals customer.CustomerId
                            where booking.Type == type
                            select new GetBookingsByTypeDto
                            {
                                BookingId = booking.BookingId,
                                Code = booking.Code,
                                RegisterDate = booking.RegisterDate,
                                Type = booking.Type,
                                CustomerFullName = customer.FullName,
                                CustomerDocumentNumber = customer.DocumentNumber
                            }).ToListAsync();

        return result;
    }
}