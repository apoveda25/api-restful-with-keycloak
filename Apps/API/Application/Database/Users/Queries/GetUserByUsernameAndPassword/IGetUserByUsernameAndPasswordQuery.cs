namespace Tarker.Booking.Application.Database.Users.Queries.GetUserByUsernameAndPassword;

public interface IGetUserByUsernameAndPasswordQuery
{
    Task<GetUserByUsernameAndPasswordDto?> ExecuteAsync(string username, string password);
}