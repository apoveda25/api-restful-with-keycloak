namespace Tarker.Booking.Application.Database.Customers.Commands.CreateCustomer;

public interface ICreateCustomerCommand
{
    Task<CreateCustomerDto> ExecuteAsync(CreateCustomerDto dto);
}