namespace Tarker.Booking.Application.Database.Bookings.Queries.GetBookingsByType;

public interface IGetBookingsByTypeQuery
{
    Task<List<GetBookingsByTypeDto>> ExecuteAsync(string type);
}