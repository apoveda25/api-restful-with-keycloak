using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Users.Commands.DeleteUser;

public class DeleteUserCommand(IDatabaseService databaseService) : IDeleteUserCommand
{
    private readonly IDatabaseService databaseService = databaseService;

    public async Task<bool> ExecuteAsync(Guid userId)
    {
        var entity = await databaseService.User.FirstOrDefaultAsync(predicate: x => x.UserId == userId);

        if (entity is null) return false;

        databaseService.User.Remove(entity: entity);

        return await databaseService.SaveAsync();
    }
}