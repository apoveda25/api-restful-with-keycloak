namespace Tarker.Booking.Application.Database.Customers.Commands.CreateCustomer;

public class CreateCustomerDto
{
    public Guid CustomerId { get; set; }
    public string FullName { get; set; }
    public string DocumentNumber { get; set; }
}