using FluentValidation;
using Tarker.Booking.Application.Database.Users.Commands.UpdateUserPassword;

namespace Tarker.Booking.Application.Validators.Users;

public class UpdateUserPasswordValidator : AbstractValidator<UpdateUserPasswordDto>
{
    public UpdateUserPasswordValidator()
    {
        RuleFor(expression: x => x.UserId).NotNull().NotEmpty();
        RuleFor(expression: x => x.Password).NotNull().NotEmpty().MaximumLength(maximumLength: 10);
    }
}