namespace Tarker.Booking.Application.Database.Customers.Queries.GetAllCustomers;

public interface IGetAllCustomersQuery
{
    Task<List<GetAllCustomersDto>> ExecuteAsync();
}