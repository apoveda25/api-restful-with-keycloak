using Tarker.Booking.API;
using Tarker.Booking.Infrastructure.Persistence.Extensions;
using Keycloak.AuthServices.Authentication;
using Keycloak.AuthServices.Authorization;
using Serilog;
using Serilog.Events;

var builder = WebApplication.CreateBuilder(args: args);

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration: builder.Configuration)
    .MinimumLevel.Override(source: "Microsoft.AspNetCore.Hosting", minimumLevel: LogEventLevel.Warning)
    .MinimumLevel.Override(source: "Microsoft.AspNetCore.Mvc", minimumLevel: LogEventLevel.Warning)
    .MinimumLevel.Override(source: "Microsoft.AspNetCore.Routing", minimumLevel: LogEventLevel.Warning)
    .WriteTo.File(path: "Logs/log.txt", rollingInterval: RollingInterval.Day, shared: true)
    .CreateBootstrapLogger();

builder.Services.AddSerilog(configureLogger: (services, lc) => lc
    .ReadFrom.Configuration(configuration: builder.Configuration)
    .MinimumLevel.Override(source: "Microsoft.AspNetCore.Hosting", minimumLevel: LogEventLevel.Warning)
    .MinimumLevel.Override(source: "Microsoft.AspNetCore.Mvc", minimumLevel: LogEventLevel.Warning)
    .MinimumLevel.Override(source: "Microsoft.AspNetCore.Routing", minimumLevel: LogEventLevel.Warning)
    .ReadFrom.Services(services: services)
    .Enrich.FromLogContext()
    .WriteTo.File(path: "Logs/log.txt", rollingInterval: RollingInterval.Day, shared: true)
);

builder.Services.AddKeycloakAuthentication(
    configuration: builder.Configuration,
    keycloakClientSectionName: "Keycloak"
);
builder.Services.AddAuthorization(configure: options =>
{
    options.AddPolicy(name: "CreateOneAttribute", configurePolicy: builder =>
    {
        builder.RequireProtectedResource(resource: "attributes-resource", scope: "create-one-scope");
    });
}).AddKeycloakAuthorization(
    configuration: builder.Configuration,
    keycloakClientSectionName: "Keycloak"
);

builder.Services
    .AddWebApi()
    .AddCommon()
    .AddApplication()
    .AddExternal(configuration: builder.Configuration)
    .AddPersistence(configuration: builder.Configuration);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors();

builder.Services.AddControllers();

var app = builder.Build();

app.UseSerilogRequestLogging();
app.UseAuthentication();
app.UseAuthorization();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    await app.ConfigureMigrations();
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(configurePolicy: options => options
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader()
);

app.MapControllers();

app.UseHttpsRedirection();
app.Run();
