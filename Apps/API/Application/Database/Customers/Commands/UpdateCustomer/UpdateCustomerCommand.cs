using AutoMapper;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Application.Database.Customers.Commands.UpdateCustomer;

public class UpdateCustomerCommand(IDatabaseService databaseService, IMapper mapper) : IUpdateCustomerCommand
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<UpdateCustomerDto> ExecuteAsync(UpdateCustomerDto dto)
    {
        var entity = mapper.Map<CustomerEntity>(source: dto);

        databaseService.Customer.Update(entity: entity);
        await databaseService.SaveAsync();

        return dto;
    }
}