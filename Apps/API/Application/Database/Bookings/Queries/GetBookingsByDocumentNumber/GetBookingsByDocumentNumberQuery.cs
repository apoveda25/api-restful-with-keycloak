using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Bookings.Queries.GetBookingsByDocumentNumber;

public class GetBookingsByDocumentNumberQuery(IDatabaseService databaseService) : IGetBookingsByDocumentNumberQuery
{
    private readonly IDatabaseService databaseService = databaseService;

    public async Task<List<GetBookingsByDocumentNumberDto>> ExecuteAsync(string documentNumber)
    {
        var result = await (from booking in databaseService.Booking
                            join customer in databaseService.Customer
                            on booking.CustomerId equals customer.CustomerId
                            where customer.DocumentNumber == documentNumber
                            select new GetBookingsByDocumentNumberDto
                            {
                                BookingId = booking.BookingId,
                                Code = booking.Code,
                                RegisterDate = booking.RegisterDate,
                                Type = booking.Type
                            }).ToListAsync();

        return result;
    }
}