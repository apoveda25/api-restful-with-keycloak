using FluentValidation;
using Tarker.Booking.Application.Database.Users.Queries.GetUserByUsernameAndPassword;

namespace Tarker.Booking.Application.Validators.Users;

public class GetUserByUsernameAndPasswordValidator : AbstractValidator<(string, string)>
{
    public GetUserByUsernameAndPasswordValidator()
    {
        RuleFor(expression: x => x.Item1).NotNull().NotEmpty().MaximumLength(maximumLength: 50);
        RuleFor(expression: x => x.Item2).NotNull().NotEmpty().MaximumLength(maximumLength: 10);
    }
}