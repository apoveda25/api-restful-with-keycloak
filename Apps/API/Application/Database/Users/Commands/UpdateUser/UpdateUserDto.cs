namespace Tarker.Booking.Application.Database.Users.Commands.UpdateUser;

public class UpdateUserDto
{
    public required Guid UserId { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? UserName { get; set; }
}