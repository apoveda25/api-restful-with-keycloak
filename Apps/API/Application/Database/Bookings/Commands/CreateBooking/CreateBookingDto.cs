namespace Tarker.Booking.Application.Database.Bookings.Commands.CreateBooking;

public class CreateBookingDto
{
    public Guid BookingId { get; set; }
    public DateTime RegisterDate { get; set; }
    public string Code { get; set; }
    public string Type { get; set; }
    public Guid CustomerId { get; set; }
    public Guid UserId { get; set; }
}