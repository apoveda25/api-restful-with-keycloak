using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Users.Commands.UpdateUser;

public class UpdateUserCommand(IDatabaseService databaseService, IMapper mapper) : IUpdateUserCommand
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<UpdateUserDto?> ExecuteAsync(UpdateUserDto dto)
    {
        var entity = await databaseService.User.FirstOrDefaultAsync(predicate: x => x.UserId == dto.UserId);

        if (entity is null) return null;

        entity.FirstName = dto.FirstName ?? entity.FirstName;
        entity.LastName = dto.LastName ?? entity.LastName;
        entity.UserName = dto.UserName ?? entity.UserName;

        await databaseService.SaveAsync();

        return mapper.Map<UpdateUserDto>(source: entity);
    }
}