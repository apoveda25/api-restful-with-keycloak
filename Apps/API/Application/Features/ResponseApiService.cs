using Tarker.Booking.Domain.Models;

namespace Tarker.Booking.Application.Features;

public static class ResponseApiService
{
    public static BaseResponseModel Response(
        int statusCode,
        object? data = null,
        string? message = ""
    )
    {
        var result = new BaseResponseModel
        {
            StatusCode = statusCode,
            Success = statusCode >= 200 && statusCode < 300,
            Message = message,
            Data = data
        };

        return result;
    }
}