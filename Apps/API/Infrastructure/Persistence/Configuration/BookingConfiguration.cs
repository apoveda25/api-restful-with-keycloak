using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Infrastructure.Persistence.Configuration;

public class BookingConfiguration : IEntityTypeConfiguration<BookingEntity>
{
    public void Configure(EntityTypeBuilder<BookingEntity> builder)
    {
        builder.ToTable(name: "Bookings");

        builder.HasKey(keyExpression: p => p.BookingId);

        builder.Property(propertyExpression: p => p.BookingId).UseCollation(collation: "utf8mb4_general_ci");

        builder.Property(propertyExpression: p => p.RegisterDate).IsRequired().HasMaxLength(maxLength: 200);

        builder.Property(propertyExpression: p => p.Code).IsRequired().HasMaxLength(maxLength: 200);

        builder.Property(propertyExpression: p => p.Type).IsRequired().HasMaxLength(maxLength: 200);

        builder.Property(propertyExpression: p => p.UserId).UseCollation(collation: "utf8mb4_general_ci").IsRequired();

        builder.Property(propertyExpression: p => p.CustomerId).UseCollation(collation: "utf8mb4_general_ci").IsRequired();

        builder
            .HasOne(navigationExpression: m => m.User)
            .WithMany(navigationExpression: o => o.Bookings)
            .HasForeignKey(foreignKeyExpression: fk => fk.UserId)
            .OnDelete(deleteBehavior: DeleteBehavior.Cascade);

        builder
            .HasOne(navigationExpression: m => m.Customer)
            .WithMany(navigationExpression: o => o.Bookings)
            .HasForeignKey(foreignKeyExpression: fk => fk.CustomerId)
            .OnDelete(deleteBehavior: DeleteBehavior.Cascade);
    }
}