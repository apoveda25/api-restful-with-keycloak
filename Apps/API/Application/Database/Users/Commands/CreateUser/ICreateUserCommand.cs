namespace Tarker.Booking.Application.Database.Users.Commands.CreateUser;

public interface ICreateUserCommand
{
    Task<CreateUserDto> ExecuteAsync(CreateUserDto dto);
}