using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Customers.Queries.GetCustomerById;

public class GetCustomerByIdQuery(IDatabaseService databaseService, IMapper mapper) : IGetCustomerByIdQuery
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<GetCustomerByIdDto?> ExecuteAsync(Guid customerId)
    {
        var entity = await databaseService.Customer.FirstOrDefaultAsync(predicate: (x) => x.CustomerId == customerId);

        return mapper.Map<GetCustomerByIdDto>(source: entity);
    }
}