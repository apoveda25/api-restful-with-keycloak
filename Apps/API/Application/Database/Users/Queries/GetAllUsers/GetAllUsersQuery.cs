using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Users.Queries.GetAllUsers;

public class GetAllUsersQuery(IDatabaseService databaseService, IMapper mapper) : IGetAllUsersQuery
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<List<GetAllUsersDto>> ExecuteAsync()
    {
        var entities = await databaseService.User.ToListAsync();

        return mapper.Map<List<GetAllUsersDto>>(source: entities);
    }
}