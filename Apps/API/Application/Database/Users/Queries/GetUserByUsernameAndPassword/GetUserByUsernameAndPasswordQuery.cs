using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Users.Queries.GetUserByUsernameAndPassword;

public class GetUserByUsernameAndPasswordQuery(IDatabaseService databaseService, IMapper mapper) : IGetUserByUsernameAndPasswordQuery
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<GetUserByUsernameAndPasswordDto?> ExecuteAsync(string username, string password)
    {
        var entity = await databaseService.User.FirstOrDefaultAsync(predicate: (x) => x.UserName == username && x.Password == password);

        return mapper.Map<GetUserByUsernameAndPasswordDto>(source: entity);
    }
}