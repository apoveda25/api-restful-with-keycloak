namespace Tarker.Booking.Application.Database.Customers.Commands.UpdateCustomer;

public interface IUpdateCustomerCommand
{
    Task<UpdateCustomerDto> ExecuteAsync(UpdateCustomerDto dto);
}