using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Tarker.Booking.Application.Features;

namespace Tarker.Booking.Application.Exceptions;

public class ExceptionManager : IExceptionFilter
{
    public void OnException(ExceptionContext context)
    {
        context.Result = new ObjectResult(value: ResponseApiService.Response(
            statusCode: StatusCodes.Status500InternalServerError,
            message: context.Exception.Message,
            data: null
        ));
    }
}