using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Customers.Commands.DeleteCustomer;

public class DeleteCustomerCommand(IDatabaseService databaseService) : IDeleteCustomerCommand
{
    private readonly IDatabaseService databaseService = databaseService;

    public async Task<bool> ExecuteAsync(Guid customerId)
    {
        var entity = await databaseService.Customer.FirstOrDefaultAsync(predicate: x => x.CustomerId == customerId);

        if (entity is null) return false;

        databaseService.Customer.Remove(entity: entity);

        return await databaseService.SaveAsync();
    }
}