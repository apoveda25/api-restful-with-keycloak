using AutoMapper;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Application.Database.Users.Commands.CreateUser;

public class CreateUserCommand(IDatabaseService databaseService, IMapper mapper) : ICreateUserCommand
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<CreateUserDto> ExecuteAsync(CreateUserDto dto)
    {
        var entity = mapper.Map<UserEntity>(source: dto);

        await databaseService.User.AddAsync(entity: entity);
        await databaseService.SaveAsync();

        dto.UserId = entity.UserId;

        return dto;
    }
}