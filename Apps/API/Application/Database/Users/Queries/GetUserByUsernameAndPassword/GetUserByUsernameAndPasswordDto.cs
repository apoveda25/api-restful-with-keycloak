namespace Tarker.Booking.Application.Database.Users.Queries.GetUserByUsernameAndPassword;

public class GetUserByUsernameAndPasswordDto
{
    public Guid UserId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
}

public class GetUserByUsernameAndPasswordInput
{

    public string UserName { get; set; }
    public string Password { get; set; }
}