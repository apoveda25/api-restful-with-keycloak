using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Users.Queries.GetUserById;

public class GetUserByIdQuery(IDatabaseService databaseService, IMapper mapper) : IGetUserByIdQuery
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<GetUserByIdDto?> ExecuteAsync(Guid userId)
    {
        var entity = await databaseService.User.FirstOrDefaultAsync(predicate: (x) => x.UserId == userId);

        return mapper.Map<GetUserByIdDto>(source: entity);
    }
}