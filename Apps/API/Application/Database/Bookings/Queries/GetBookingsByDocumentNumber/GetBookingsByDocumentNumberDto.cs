namespace Tarker.Booking.Application.Database.Bookings.Queries.GetBookingsByDocumentNumber;

public class GetBookingsByDocumentNumberDto
{
    public Guid BookingId { get; set; }
    public DateTime RegisterDate { get; set; }
    public string Code { get; set; }
    public string Type { get; set; }
}