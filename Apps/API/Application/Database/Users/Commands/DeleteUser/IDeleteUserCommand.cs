namespace Tarker.Booking.Application.Database.Users.Commands.DeleteUser;

public interface IDeleteUserCommand
{
    Task<bool> ExecuteAsync(Guid userId);
}