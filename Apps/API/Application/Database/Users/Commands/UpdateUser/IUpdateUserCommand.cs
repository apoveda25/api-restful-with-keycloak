namespace Tarker.Booking.Application.Database.Users.Commands.UpdateUser;

public interface IUpdateUserCommand
{
    Task<UpdateUserDto> ExecuteAsync(UpdateUserDto dto);
}