namespace Tarker.Booking.Application.Database.Customers.Commands.UpdateCustomer;

public class UpdateCustomerDto
{
    public Guid CustomerId { get; set; }
    public string FullName { get; set; }
    public string DocumentNumber { get; set; }
}