using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Users.Commands.UpdateUserPassword;

public class UpdateUserPasswordCommand(IDatabaseService databaseService) : IUpdateUserPasswordCommand
{
    private readonly IDatabaseService databaseService = databaseService;

    public async Task<bool> ExecuteAsync(UpdateUserPasswordDto dto)
    {
        var entity = await databaseService.User.FirstOrDefaultAsync(predicate: x => x.UserId == dto.UserId);

        if (entity is null) return false;

        entity.Password = dto.Password;

        return await databaseService.SaveAsync();
    }
}