namespace Tarker.Booking.Application.Database.Users.Commands.UpdateUserPassword;

public interface IUpdateUserPasswordCommand
{
    Task<bool> ExecuteAsync(UpdateUserPasswordDto dto);
}