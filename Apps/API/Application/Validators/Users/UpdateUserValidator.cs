using FluentValidation;
using Tarker.Booking.Application.Database.Users.Commands.UpdateUser;

namespace Tarker.Booking.Application.Validators.Users;

public class UpdateUserValidator : AbstractValidator<UpdateUserDto>
{
    public UpdateUserValidator()
    {
        RuleFor(expression: x => x.UserId).NotNull().NotEmpty();
        RuleFor(expression: x => x.FirstName).NotNull().NotEmpty().MaximumLength(maximumLength: 50);
        RuleFor(expression: x => x.LastName).NotNull().NotEmpty().MaximumLength(maximumLength: 50);
        RuleFor(expression: x => x.UserName).NotNull().NotEmpty().MaximumLength(maximumLength: 50);
    }
}