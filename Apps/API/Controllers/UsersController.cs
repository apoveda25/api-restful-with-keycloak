using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tarker.Booking.Application.Database.Users.Commands.CreateUser;
using Tarker.Booking.Application.Database.Users.Commands.DeleteUser;
using Tarker.Booking.Application.Database.Users.Commands.UpdateUser;
using Tarker.Booking.Application.Database.Users.Commands.UpdateUserPassword;
using Tarker.Booking.Application.Database.Users.Queries.GetAllUsers;
using Tarker.Booking.Application.Database.Users.Queries.GetUserById;
using Tarker.Booking.Application.Database.Users.Queries.GetUserByUsernameAndPassword;
using Tarker.Booking.Application.Exceptions;
using Tarker.Booking.Application.Features;

namespace Tarker.Booking.API.Controllers;

[ApiController]
[Route(template: "/api/v1/users")]

[TypeFilter(type: typeof(ExceptionManager))]
public class UsersController : ControllerBase
{

    [Authorize(Policy = "CreateOneAttribute")]
    [HttpPost]
    public async Task<IActionResult> Create(
        [FromBody] CreateUserDto dto,
        [FromServices] ICreateUserCommand command,
        [FromServices] IValidator<CreateUserDto> validator
    )
    {
        var validate = await validator.ValidateAsync(instance: dto);

        if (!validate.IsValid)
        {
            return StatusCode(
                statusCode: StatusCodes.Status400BadRequest,
                value: ResponseApiService.Response(
                    statusCode: StatusCodes.Status400BadRequest,
                    message: validate.Errors.ConvertAll(converter: x => x.ErrorMessage ?? "").Aggregate(func: (x, y) => x + ", " + y)
                )
            );
        }

        var data = await command.ExecuteAsync(dto: dto);

        return StatusCode(
            statusCode: StatusCodes.Status201Created,
            value: ResponseApiService.Response(
                statusCode: StatusCodes.Status201Created,
                data: data
            )
        );
    }

    [HttpPut]
    public async Task<IActionResult> Update(
        [FromBody] UpdateUserDto dto,
        [FromServices] IUpdateUserCommand command,
        [FromServices] IValidator<UpdateUserDto> validator
    )
    {
        var validate = await validator.ValidateAsync(instance: dto);

        if (!validate.IsValid)
        {
            return StatusCode(
                statusCode: StatusCodes.Status400BadRequest,
                value: ResponseApiService.Response(
                    statusCode: StatusCodes.Status400BadRequest,
                    message: validate.Errors.ConvertAll(converter: x => x.ErrorMessage ?? "").Aggregate(func: (x, y) => x + ", " + y)
                )
            );
        }

        var data = await command.ExecuteAsync(dto: dto);

        return StatusCode(
            statusCode: StatusCodes.Status200OK,
            value: ResponseApiService.Response(
                statusCode: StatusCodes.Status200OK,
                data: data
            )
        );
    }

    [HttpPatch]
    public async Task<IActionResult> UpdatePassword(
        [FromBody] UpdateUserPasswordDto dto,
        [FromServices] IUpdateUserPasswordCommand command,
        [FromServices] IValidator<UpdateUserPasswordDto> validator
    )
    {
        var validate = await validator.ValidateAsync(instance: dto);

        if (!validate.IsValid)
        {
            return StatusCode(
                statusCode: StatusCodes.Status400BadRequest,
                value: ResponseApiService.Response(
                    statusCode: StatusCodes.Status400BadRequest,
                    message: validate.Errors.ConvertAll(converter: x => x.ErrorMessage ?? "").Aggregate(func: (x, y) => x + ", " + y)
                )
            );
        }

        var data = await command.ExecuteAsync(dto: dto);

        return StatusCode(
            statusCode: StatusCodes.Status200OK,
            value: ResponseApiService.Response(
                statusCode: StatusCodes.Status200OK,
                data: data
            )
        );
    }

    [HttpDelete(template: "{userId}")]
    public async Task<IActionResult> Delete(
        [FromRoute] Guid userId,
        [FromServices] IDeleteUserCommand command
    )
    {
        var data = await command.ExecuteAsync(userId: userId);

        if (data is false)
            return StatusCode(
               statusCode: StatusCodes.Status404NotFound,
               value: ResponseApiService.Response(
                   statusCode: StatusCodes.Status404NotFound,
                   data: data
               )
           );

        return StatusCode(
            statusCode: StatusCodes.Status200OK,
            value: ResponseApiService.Response(
                statusCode: StatusCodes.Status200OK,
                data: data
            )
        );
    }

    [HttpGet]
    public async Task<IActionResult> GetAll(
        [FromServices] IGetAllUsersQuery query
    )
    {
        var data = await query.ExecuteAsync();

        return StatusCode(
            statusCode: StatusCodes.Status200OK,
            value: ResponseApiService.Response(
                statusCode: StatusCodes.Status200OK,
                data: data
            )
        );
    }

    [HttpGet(template: "{userId}")]
    public async Task<IActionResult> GetById(
        [FromRoute] Guid userId,
        [FromServices] IGetUserByIdQuery query
    )
    {
        var data = await query.ExecuteAsync(userId: userId);

        return StatusCode(
            statusCode: StatusCodes.Status200OK,
            value: ResponseApiService.Response(
                statusCode: StatusCodes.Status200OK,
                data: data
            )
        );
    }

    [HttpGet(template: "{username}/password/{password}")]
    public async Task<IActionResult> GetByUsernameAndPassword(
        // [FromRoute] string username,
        // [FromRoute] string password,
        [FromRoute] GetUserByUsernameAndPasswordInput dto,
        [FromServices] IGetUserByUsernameAndPasswordQuery query,
        [FromServices] IValidator<(string, string)> validator
    )
    {
        var validate = await validator.ValidateAsync(instance: (dto.UserName, dto.Password));

        if (!validate.IsValid)
        {
            return StatusCode(
                statusCode: StatusCodes.Status400BadRequest,
                value: ResponseApiService.Response(
                    statusCode: StatusCodes.Status400BadRequest,
                    message: validate.Errors.ConvertAll(converter: x => x.ErrorMessage ?? "").Aggregate(func: (x, y) => x + ", " + y)
                )
            );
        }

        var data = await query.ExecuteAsync(username: dto.UserName, password: dto.Password);

        return StatusCode(
            statusCode: StatusCodes.Status200OK,
            value: ResponseApiService.Response(
                statusCode: StatusCodes.Status200OK,
                data: data
            )
        );
    }
}