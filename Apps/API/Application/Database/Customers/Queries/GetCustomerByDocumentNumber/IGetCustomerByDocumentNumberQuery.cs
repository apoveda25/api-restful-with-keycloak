namespace Tarker.Booking.Application.Database.Customers.Queries.GetCustomerByDocumentNumber;

public interface IGetCustomerByDocumentNumberQuery
{
    Task<GetCustomerByDocumentNumberDto?> ExecuteAsync(string documentNumber);
}