namespace Tarker.Booking.Domain.Entities;

public class CustomerEntity
{
    public Guid CustomerId { get; set; }
    public string FullName { get; set; }
    public string DocumentNumber { get; set; }
    public ICollection<BookingEntity> Bookings { get; set; }
}