using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Tarker.Booking.Application.Database.Customers.Queries.GetCustomerByDocumentNumber;

public class GetCustomerByDocumentNumberQuery(IDatabaseService databaseService, IMapper mapper) : IGetCustomerByDocumentNumberQuery
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<GetCustomerByDocumentNumberDto?> ExecuteAsync(string documentNumber)
    {
        var entity = await databaseService.Customer.FirstOrDefaultAsync(predicate: (x) => x.DocumentNumber == documentNumber);

        return mapper.Map<GetCustomerByDocumentNumberDto>(source: entity);
    }
}