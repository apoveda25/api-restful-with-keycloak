using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Infrastructure.Persistence.Configuration;

public class CustomerConfiguration : IEntityTypeConfiguration<CustomerEntity>
{
    public void Configure(EntityTypeBuilder<CustomerEntity> builder)
    {
        builder.ToTable(name: "Customers");

        builder.HasKey(keyExpression: p => p.CustomerId);

        builder.Property(propertyExpression: p => p.CustomerId).UseCollation(collation: "utf8mb4_general_ci");

        builder.Property(propertyExpression: p => p.FullName).IsRequired().HasMaxLength(maxLength: 200);

        builder.Property(propertyExpression: p => p.DocumentNumber).IsRequired().HasMaxLength(maxLength: 200);

        builder
            .HasMany(navigationExpression: m => m.Bookings)
            .WithOne(navigationExpression: o => o.Customer)
            .HasForeignKey(foreignKeyExpression: fk => fk.CustomerId)
            .OnDelete(deleteBehavior: DeleteBehavior.Cascade);
    }
}