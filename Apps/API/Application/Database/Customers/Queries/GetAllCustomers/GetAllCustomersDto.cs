namespace Tarker.Booking.Application.Database.Customers.Queries.GetAllCustomers;

public class GetAllCustomersDto
{
    public Guid CustomerId { get; set; }
    public string FullName { get; set; }
    public string DocumentNumber { get; set; }
}