namespace Tarker.Booking.Application.Database.Customers.Commands.DeleteCustomer;

public interface IDeleteCustomerCommand
{
    Task<bool> ExecuteAsync(Guid customerId);
}