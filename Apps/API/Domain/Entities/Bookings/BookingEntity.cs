namespace Tarker.Booking.Domain.Entities;

public class BookingEntity
{
    public Guid BookingId { get; set; }
    public DateTime RegisterDate { get; set; }
    public string Code { get; set; }
    public string Type { get; set; }
    public Guid CustomerId { get; set; }
    public Guid UserId { get; set; }
    public UserEntity User { get; set; }
    public CustomerEntity Customer { get; set; }
}