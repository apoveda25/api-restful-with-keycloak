namespace Tarker.Booking.Application.Database.Users.Queries.GetAllUsers;

public interface IGetAllUsersQuery
{
    Task<List<GetAllUsersDto>> ExecuteAsync();
}