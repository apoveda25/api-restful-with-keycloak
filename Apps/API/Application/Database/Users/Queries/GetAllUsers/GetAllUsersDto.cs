namespace Tarker.Booking.Application.Database.Users.Queries.GetAllUsers;

public class GetAllUsersDto
{
    public Guid UserId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string UserName { get; set; }
}