using AutoMapper;
using Tarker.Booking.Application.Database.Bookings.Commands.CreateBooking;
using Tarker.Booking.Application.Database.Bookings.Queries.GetAllBookings;
using Tarker.Booking.Application.Database.Customers.Commands.CreateCustomer;
using Tarker.Booking.Application.Database.Customers.Commands.UpdateCustomer;
using Tarker.Booking.Application.Database.Customers.Queries.GetAllCustomers;
using Tarker.Booking.Application.Database.Customers.Queries.GetCustomerByDocumentNumber;
using Tarker.Booking.Application.Database.Customers.Queries.GetCustomerById;
using Tarker.Booking.Application.Database.Users.Commands.CreateUser;
using Tarker.Booking.Application.Database.Users.Commands.UpdateUser;
using Tarker.Booking.Application.Database.Users.Queries.GetAllUsers;
using Tarker.Booking.Application.Database.Users.Queries.GetUserById;
using Tarker.Booking.Application.Database.Users.Queries.GetUserByUsernameAndPassword;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Application.Configuration;

public class MapperProfile : Profile
{
    public MapperProfile()
    {
        CreateMap<UserEntity, CreateUserDto>().ReverseMap().ForMember(
            destinationMember: dest => dest.UserId,
            memberOptions: opt => opt.NullSubstitute(nullSubstitute: Guid.NewGuid())
        );
        CreateMap<UserEntity, UpdateUserDto>().ReverseMap();
        CreateMap<UserEntity, GetAllUsersDto>().ReverseMap();
        CreateMap<UserEntity, GetUserByIdDto>().ReverseMap();
        CreateMap<UserEntity, GetUserByUsernameAndPasswordDto>().ReverseMap();

        CreateMap<CustomerEntity, CreateCustomerDto>().ReverseMap();
        CreateMap<CustomerEntity, UpdateCustomerDto>().ReverseMap();
        CreateMap<CustomerEntity, GetAllCustomersDto>().ReverseMap();
        CreateMap<CustomerEntity, GetCustomerByIdDto>().ReverseMap();
        CreateMap<CustomerEntity, GetCustomerByDocumentNumberDto>().ReverseMap();

        CreateMap<BookingEntity, CreateBookingDto>().ReverseMap();
    }
}
