using AutoMapper;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Application.Database.Bookings.Commands.CreateBooking;

public class CreateBookingCommand(IDatabaseService databaseService, IMapper mapper) : ICreateBookingCommand
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<CreateBookingDto> ExecuteAsync(CreateBookingDto dto)
    {
        var entity = mapper.Map<BookingEntity>(source: dto);

        await databaseService.Booking.AddAsync(entity: entity);
        await databaseService.SaveAsync();

        return dto;
    }
}