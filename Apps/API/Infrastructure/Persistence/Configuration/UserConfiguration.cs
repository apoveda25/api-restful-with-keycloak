using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Infrastructure.Persistence.Configuration;

public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
{
    public void Configure(EntityTypeBuilder<UserEntity> builder)
    {
        builder.ToTable(name: "Users");

        builder.HasKey(keyExpression: p => p.UserId);

        builder.Property(propertyExpression: p => p.UserId).UseCollation(collation: "utf8mb4_general_ci");

        builder.Property(propertyExpression: p => p.FirstName).IsRequired().HasMaxLength(maxLength: 200);

        builder.Property(propertyExpression: p => p.LastName).IsRequired().HasMaxLength(maxLength: 200);

        builder.Property(propertyExpression: p => p.UserName).IsRequired().HasMaxLength(maxLength: 200);

        builder.Property(propertyExpression: p => p.Password).IsRequired().HasMaxLength(maxLength: 200);

        builder
            .HasMany(navigationExpression: m => m.Bookings)
            .WithOne(navigationExpression: o => o.User)
            .HasForeignKey(foreignKeyExpression: fk => fk.UserId)
            .OnDelete(deleteBehavior: DeleteBehavior.Cascade);
    }
}