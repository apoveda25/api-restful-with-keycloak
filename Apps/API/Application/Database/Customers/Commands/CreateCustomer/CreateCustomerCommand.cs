using AutoMapper;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Application.Database.Customers.Commands.CreateCustomer;

public class CreateCustomerCommand(IDatabaseService databaseService, IMapper mapper) : ICreateCustomerCommand
{
    private readonly IDatabaseService databaseService = databaseService;
    private readonly IMapper mapper = mapper;

    public async Task<CreateCustomerDto> ExecuteAsync(CreateCustomerDto dto)
    {
        var entity = mapper.Map<CustomerEntity>(source: dto);

        await databaseService.Customer.AddAsync(entity: entity);
        await databaseService.SaveAsync();

        return dto;
    }
}