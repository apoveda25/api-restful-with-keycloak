using Microsoft.EntityFrameworkCore;
using Tarker.Booking.Domain.Entities;

namespace Tarker.Booking.Application.Database;

public interface IDatabaseService
{
    DbSet<UserEntity> User { get; set; }
    DbSet<CustomerEntity> Customer { get; set; }
    DbSet<BookingEntity> Booking { get; set; }

    Task<bool> SaveAsync();
}