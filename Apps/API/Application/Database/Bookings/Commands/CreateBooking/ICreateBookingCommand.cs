namespace Tarker.Booking.Application.Database.Bookings.Commands.CreateBooking;

public interface ICreateBookingCommand
{
    Task<CreateBookingDto> ExecuteAsync(CreateBookingDto dto);
}