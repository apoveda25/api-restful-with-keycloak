using FluentValidation;
using Tarker.Booking.Application.Database.Users.Commands.CreateUser;

namespace Tarker.Booking.Application.Validators.Users;

public class CreateUserValidator : AbstractValidator<CreateUserDto>
{
    public CreateUserValidator()
    {
        RuleFor(expression: x => x.FirstName).NotNull().NotEmpty().MaximumLength(maximumLength: 50);
        RuleFor(expression: x => x.LastName).NotNull().NotEmpty().MaximumLength(maximumLength: 50);
        RuleFor(expression: x => x.UserName).NotNull().NotEmpty().MaximumLength(maximumLength: 50);
        RuleFor(expression: x => x.Password).NotNull().NotEmpty().MaximumLength(maximumLength: 10);
    }
}