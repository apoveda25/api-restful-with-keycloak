using AutoMapper;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Tarker.Booking.Application.Configuration;
using Tarker.Booking.Application.Database;
using Tarker.Booking.Application.Database.Bookings.Commands.CreateBooking;
using Tarker.Booking.Application.Database.Bookings.Queries.GetBookingsByDocumentNumber;
using Tarker.Booking.Application.Database.Bookings.Queries.GetBookingsByType;
using Tarker.Booking.Application.Database.Customers.Commands.CreateCustomer;
using Tarker.Booking.Application.Database.Customers.Commands.DeleteCustomer;
using Tarker.Booking.Application.Database.Customers.Commands.UpdateCustomer;
using Tarker.Booking.Application.Database.Customers.Queries.GetAllCustomers;
using Tarker.Booking.Application.Database.Customers.Queries.GetCustomerByDocumentNumber;
using Tarker.Booking.Application.Database.Customers.Queries.GetCustomerById;
using Tarker.Booking.Application.Database.Users.Commands.CreateUser;
using Tarker.Booking.Application.Database.Users.Commands.DeleteUser;
using Tarker.Booking.Application.Database.Users.Commands.UpdateUser;
using Tarker.Booking.Application.Database.Users.Commands.UpdateUserPassword;
using Tarker.Booking.Application.Database.Users.Queries.GetAllUsers;
using Tarker.Booking.Application.Database.Users.Queries.GetUserById;
using Tarker.Booking.Application.Database.Users.Queries.GetUserByUsernameAndPassword;
using Tarker.Booking.Application.Validators.Users;
using Tarker.Booking.Infrastructure.Persistence.Database;

namespace Tarker.Booking.API;

public static class DependencyInjectionService
{
    public static IServiceCollection AddWebApi(this IServiceCollection services)
    {
        return services;
    }

    public static IServiceCollection AddCommon(this IServiceCollection services)
    {
        return services;
    }

    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        var mapper = new MapperConfiguration(configure: config => config.AddProfile(profile: new MapperProfile()));

        services.AddSingleton(implementationInstance: mapper.CreateMapper());
        services.AddTransient<ICreateUserCommand, CreateUserCommand>();
        services.AddTransient<IUpdateUserCommand, UpdateUserCommand>();
        services.AddTransient<IDeleteUserCommand, DeleteUserCommand>();
        services.AddTransient<IUpdateUserPasswordCommand, UpdateUserPasswordCommand>();
        services.AddTransient<IGetAllUsersQuery, GetAllUsersQuery>();
        services.AddTransient<IGetUserByIdQuery, GetUserByIdQuery>();
        services.AddTransient<IGetUserByUsernameAndPasswordQuery, GetUserByUsernameAndPasswordQuery>();

        services.AddTransient<ICreateCustomerCommand, CreateCustomerCommand>();
        services.AddTransient<IUpdateCustomerCommand, UpdateCustomerCommand>();
        services.AddTransient<IDeleteCustomerCommand, DeleteCustomerCommand>();
        services.AddTransient<IGetAllCustomersQuery, GetAllCustomersQuery>();
        services.AddTransient<IGetCustomerByIdQuery, GetCustomerByIdQuery>();
        services.AddTransient<IGetCustomerByDocumentNumberQuery, GetCustomerByDocumentNumberQuery>();

        services.AddTransient<ICreateBookingCommand, CreateBookingCommand>();
        services.AddTransient<IGetBookingsByDocumentNumberQuery, GetBookingsByDocumentNumberQuery>();
        services.AddTransient<IGetBookingsByTypeQuery, GetBookingsByTypeQuery>();

        services.AddScoped<IValidator<CreateUserDto>, CreateUserValidator>();
        services.AddScoped<IValidator<UpdateUserDto>, UpdateUserValidator>();
        services.AddScoped<IValidator<UpdateUserPasswordDto>, UpdateUserPasswordValidator>();
        services.AddScoped<IValidator<(string, string)>, GetUserByUsernameAndPasswordValidator>();

        return services;
    }

    public static IServiceCollection AddExternal(this IServiceCollection services, IConfiguration configuration)
    {
        return services;
    }

    public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
    {
        var databaseConnectionString = configuration.GetConnectionString(name: "TarkerBookingConnectionUrl");

        services.AddDbContext<DatabaseService>(
            optionsAction: options => options.UseMySql(
                connectionString: databaseConnectionString,
                serverVersion: ServerVersion.AutoDetect(connectionString: databaseConnectionString),
                mySqlOptionsAction: x => x.MigrationsHistoryTable(tableName: HistoryRepository.DefaultTableName)
            )
        );

        services.AddScoped<IDatabaseService, DatabaseService>();

        return services;
    }
}