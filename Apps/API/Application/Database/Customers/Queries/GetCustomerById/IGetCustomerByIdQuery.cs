namespace Tarker.Booking.Application.Database.Customers.Queries.GetCustomerById;

public interface IGetCustomerByIdQuery
{
    Task<GetCustomerByIdDto?> ExecuteAsync(Guid customerId);
}